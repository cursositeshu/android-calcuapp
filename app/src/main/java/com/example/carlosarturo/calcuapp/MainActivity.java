package com.example.carlosarturo.calcuapp;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {
    private Float _operador1;
    private Float _operador2;
    private EditText txtOperador1;
    private EditText txtOperador2;
    private TextView lblResultado;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        txtOperador1=(EditText) findViewById(R.id.txtOperador1);
        txtOperador2=(EditText) findViewById(R.id.txtOperador2);
        lblResultado=(TextView) findViewById(R.id.lblResultado);

        Button btnSuma=(Button)findViewById(R.id.btnSuma);
        Button btnResta=(Button)findViewById(R.id.btnResta);
        Button btnMultiplicacion=(Button) findViewById(R.id.btnMultiplicacion);
        Button btnDivision=(Button) findViewById(R.id.btnDivision);
        Button btnSqrt=(Button) findViewById(R.id.btnSqrt);
        Button btnCF=(Button) findViewById(R.id.btnCentigradosFarenheit);
        Button btnCK=(Button) findViewById(R.id.btnCentigradosKelvin);
        Button btnFC=(Button) findViewById(R.id.btnFarengeitCentigrados);
        Button btnFK=(Button) findViewById(R.id.btnFarengeitKelvin);
        Button btnKC=(Button) findViewById(R.id.btnKelvinCentigrados);
        Button btnKF=(Button) findViewById(R.id.btnKelvinFarengeit);


            btnSuma.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(ValidarOperacionBinaria())
                    lblResultado.setText(Suma().toString());
                }
            });
            btnResta.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(ValidarOperacionBinaria())
                    lblResultado.setText(Resta().toString());
                }
            });
            btnMultiplicacion.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(ValidarOperacionBinaria())
                    lblResultado.setText(Multiplicacion().toString());
                }
            });
            btnDivision.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(ValidarOperacionBinaria())
                    if (Float.parseFloat(txtOperador2.getText().toString())!=0){
                        lblResultado.setText(Division().toString());
                    }else{
                        lblResultado.setText("Indeterminado");
                    }
                }
            });
            btnSqrt.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(ValidarOperacionUnaria())
                    lblResultado.setText(Sqrt().toString());
                }
            });
            btnCF.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(ValidarOperacionUnaria())
                    lblResultado.setText(CentigradosFahrenheit().toString());
                }
            });
            btnCK.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(ValidarOperacionUnaria())
                    lblResultado.setText(CentigradosKelvin().toString());
                }
            });
            btnFC.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(ValidarOperacionUnaria())
                    lblResultado.setText(CentigradosFahrenheit().toString());
                }
            });
            btnFK.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(ValidarOperacionUnaria())
                    lblResultado.setText(CentigradosKelvin().toString());
                }
            });
            btnKC.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(ValidarOperacionUnaria())
                    lblResultado.setText(KelvinCentigrados().toString());
                }
            });
            btnKF.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(ValidarOperacionUnaria())
                        lblResultado.setText(KelvinFahrenheit().toString());
                }
            });
    }

    public boolean ValidarOperacionBinaria(){
        if(txtOperador1.getText().length()>0 && txtOperador2.getText().length()>0)
        {
            this._operador1=Float.parseFloat(txtOperador1.getText().toString());
            this._operador2=Float.parseFloat(txtOperador2.getText().toString());
            return true;
        }else
        {
            Toast.makeText(getApplicationContext(),"Datos Faltantes",Toast.LENGTH_SHORT).show();
            return false;
        }

    }
    public boolean ValidarOperacionUnaria(){
        if(txtOperador1.getText().length()>0)
        {
            this._operador1=Float.parseFloat(txtOperador1.getText().toString());
            return true;
        }else
        {
            Toast.makeText(getApplicationContext(),"Datos Faltantes",Toast.LENGTH_SHORT).show();
            return false;
        }

    }

    public Float Suma(){
        return this._operador1+this._operador2;
    }

    public Float Resta(){
        return this._operador1-this._operador2;
    }
    public Float Multiplicacion()
    {
        return this._operador1*this._operador2;
    }
    public Float Division()
    {
        return this._operador1/this._operador2;
    }
    public Double Sqrt()
    {
        return  Math.sqrt(this._operador1.doubleValue());
    }
    public Float FahrenheitCentigrados(){
        return (5*(this._operador1-32))/9;
    }
    public  Float CentigradosFahrenheit(){
        return (9*this._operador1/5)+32;
    }
    public Float KelvinCentigrados(){
        return this._operador1-273.15F;
    }
    public Float CentigradosKelvin()
    {
        return this._operador1+273.15F;
    }
    public Float KelvinFahrenheit(){
        return (9*(this._operador1-273.15F)/5)+32;
    }
    public Float FahrenheitKelvin()
    {
        return (5*(this._operador1-32)/9)+273.15F;
    }


}
